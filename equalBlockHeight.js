(function () {
	"use strict";
	/**
	 * Tam Tam equalBlockHeight Angular Directive
	 * @version v1.0.2
	 * @link https://bitbucket.org/tamtam-nl/tamtam-bower-equal-block-height
	 * @author Elmar Kouwenhoven <elmar@tamtam.nl>
	 * @license MIT License, http://www.opensource.org/licenses/MIT
	 */

	var module = angular.module('tamtam.equalBlockHeight', []);

	module.directive('equalBlockHeight', ['$timeout', function (timeout) {

		var counter = 0;
		var elements = {};

		return {

			compile: function (element, attr) {

				// Count all blockHeight directives
				counter++;

				function calculateHighest() {

					var highest = {};

					angular.forEach(elements, function (elementWithHeightArray, keyword) {

						angular.forEach(elementWithHeightArray, function (elementWithHeight, index) {

							if (highest[keyword] == null) {
								highest[keyword] = 0;
							}

							if (elementWithHeight.height > highest[keyword]) {
								highest[keyword] = elementWithHeight.height;
							}

						});

					});

					return highest;
				}

				function setHighest(highest) {
					angular.forEach(elements, function (elementWithHeightArray, keyword) {
						angular.forEach(elementWithHeightArray, function (elementWithHeight, index) {
							elementWithHeight.element.css("min-height", highest[keyword]);
						});
					});
				}


				return function (scope, element, attrs) {

					timeout(function () {

						var keyword = attr.equalBlockHeights;

						if (elements[keyword] == null) {
							elements[keyword] = [];
						}

						elements[keyword].push({
							element: element,
							height: element.innerHeight()
						});

						counter--;

						if (counter == 0) {
							var calculatedHeight = calculateHighest();
							setHighest(calculatedHeight);
						}

					}, 0, false);
				}
			}
		}

	}])

})();
