ng-equal-block-height
==================

Simple Angular Directive for setting a specified block to highest blockheight.

## Usage:
* download, clone or fork it or install it using [bower](http://twitter.github.com/bower/) `bower install tamtam-equalBlockHeight-directive`
* Include the `equalBlockHeight.js` script provided by this component into your app.
* Add `'tamtam.equalBlockHeight'` as a module dependency to your app: `angular.module('app', ['tamtam.equalBlockHeight'])`


## Example
```html
<div equal-block-height="keyword1"></div>
<div equal-block-height="keyword1"></div>
```